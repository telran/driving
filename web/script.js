(function(angular) {
    'use strict';
    var auth = {login:'login',password:'password'};
    var token = {id:0};
    var app =  angular.module('Site', ['ngRoute', 'ngAnimate'])
        .config(['$routeProvider', '$locationProvider',
            function($routeProvider, $locationProvider) {
                $routeProvider
                    .when('/Login', {
                        templateUrl: 'loginForm.html',
                        controller: 'loginForm',
                        controllerAs: 'login',
                        resolve: {
                            validate : function (validatorService){
                                validatorService.getAuth();
                            }
                        }
                    })
                    .when('/Register', {
                        templateUrl: 'register.html',
                        controller: 'registerCtrl',
                        controllerAs: 'register'
                    });

                $locationProvider.html5Mode(true);
            }])
        .controller('MainCtrl', ['$route', '$routeParams', '$location',
            function($route, $routeParams, $location) {
                this.$route = $route;
                this.$location = $location;
                this.$routeParams = $routeParams;
            }
        ])
        .controller('loginForm', ['$routeParams',
            function($routeParams) {
                this.name = "loginForm";
                this.params = $routeParams;
            }
        ])
        .controller('registerCtrl', ['$routeParams',
            function($routeParams) {
                this.name = "registerCtrl";
                this.params = $routeParams;
            }
        ]);

    app.factory("validatorService", function($http){
        return {
            getAuth: function(){
                if (token.id != 0) $http.post('someserver/api' ,token.id).then(function (response) {
                    if (response.answer == 1){
                        token.id = response.id;
                        resolve();
                    }
                    else reject ('Authorization Failed')

                }, function (response) {
                    reject('Network Error :' + response.status)
                });
                else $http.post('someserver/api' ,auth).then(function (response) {
                    if (response.answer == 1){
                        token.id = response.id;
                        resolve();
                    }
                    else reject ('Authorization Failed')
                })
            }
        };

    });

    app
        .controller('LoginController', function($scope,$http) {
            $scope.send = function(){
                var loginData = {login : $scope.login, password : $scope.password};
                console.log(loginData);
                $http.post('someserver/api', JSON.stringify(loginData)).then(function(response) {
                        console.log(response.data);
                        if(response.data ==0) $scope.result = 'Login or Password incorrect';
                        else {
                            document.getElementById('loginForm').style.display = "none";
                            document.getElementById('content').style.display = "block";
                            $scope.id = response.data;
                        }
                    },
                    function(response){ // optional
                        console.log(response)
                    });
            };
        });
})(window.angular);