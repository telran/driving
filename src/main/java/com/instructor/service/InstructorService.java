package com.instructor.service;

import com.instructor.dao.Instructor;
import com.instructor.dao.ServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InstructorService {

    @Autowired
    private ServiceDao dao;

    @RequestMapping("/getInstructorById/{id}")
    public Instructor getInstructorById(@PathVariable("id") int id) {
        Instructor i = dao.getInstructor(id);
        return i;
    }

    @RequestMapping("/getInstructorByTypeVehicle/{TypeVehicle}")
    public List<Instructor> getInstructorByTypeVehicle(@PathVariable("TypeVehicle") int TypeVehicle) {
        List<Instructor> i = dao.getInstructorByTypeVehicle(TypeVehicle);
        return i;
    }

    @RequestMapping("/getInstructorByTransmission/{transmission}")
    public List<Instructor> getInstructorByTransmission(@PathVariable("transmission") int transmission) {
        List<Instructor> i = dao.getInstructorByTransmission(transmission);
        return i;
    }

    @RequestMapping("/getInstructorBySex/{sex}")
    public List<Instructor> getInstructorBySex(@PathVariable("sex") int sex) {
        List<Instructor> i = dao.getInstructorBySex(sex);
        return i;
    }

    @RequestMapping("/getInstructorByRegion/{region}")
    public List<Instructor> getInstructorByArea(@PathVariable("region") int region) {
        List<Instructor> i = dao.getInstructorByArea(region);
        return i;
    }

    @RequestMapping("/getInstructorByRegionAndCity/{region}/{city}")
    public List<Instructor> getInstructorByAreaAndCity(@PathVariable("region") int region, @PathVariable("city") int city) {
        List<Instructor> i = dao.getInstructorByCity(region,city);
        return i;
    }

    @RequestMapping(value="/addInstructor" ,method = RequestMethod.POST)
    public Integer addInstructor(@RequestBody Instructor instructor) {
        return dao.addInstructor(instructor);
    }

}
