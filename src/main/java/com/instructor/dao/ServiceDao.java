package com.instructor.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ServiceDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Instructor getInstructor(int id){
        return entityManager.find(Instructor.class,id);
    }

    @Transactional
    public List<Instructor> getInstructorByTypeVehicle(int typeVehicle){
        Query query = entityManager.createQuery("SELECT i FROM com.instructor.dao.Instructor AS i WHERE i.typeVehicle="+typeVehicle);
        List<Instructor> rs = query.getResultList();
        return rs;
    }

    @Transactional
    public List<Instructor> getInstructorByTransmission(int transmission){
        Query query = entityManager.createQuery("select i from com.instructor.dao.Instructor i where i.transmission="+transmission);
        List<Instructor> rs = query.getResultList();
        return rs;
    }

    @Transactional
    public List<Instructor> getInstructorBySex(int sex){
        Query query = entityManager.createQuery("select i from com.instructor.dao.Instructor i where i.sex="+sex);
        List<Instructor> rs = query.getResultList();
        return rs;
    }

    @Transactional
    public List<Instructor> getInstructorByArea(int region){
        Query query = entityManager.createQuery("select i from com.instructor.dao.Instructor as i where i.region="+region);
        List<Instructor> rs = query.getResultList();
        return rs;
    }

    @Transactional
    public List<Instructor> getInstructorByCity(int region, int city){
        Query query = entityManager.createQuery("select i from com.instructor.dao.Instructor i where region="+region +"and city="+ city);
        List<Instructor> rs = query.getResultList();
        return rs;
    }

    @Transactional
    public int addInstructor(Instructor instructor){

        entityManager.persist(instructor);
        return 1;

    }

/* (String email, int typeVehicle, int transmission, int sex, int region,
*int city , String name , String phone, String password, String drivingSchool, String workingDays,
*    String workingHours, int pricePerHour, String url, String avatar)
*/
}
